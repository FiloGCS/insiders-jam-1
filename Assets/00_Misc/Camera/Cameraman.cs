﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cameraman : MonoBehaviour {
    public Transform target;
    public float speed = 5;

    private void Start() {
        if (GM.instance.player) {
            target = GM.instance.player.transform;
            this.transform.position = target.position;
        }
    }

    private void Update() {
        if (target) {
            this.transform.position = Vector3.Lerp(this.transform.position,target.position, speed*Time.deltaTime);
        }
    }

}
