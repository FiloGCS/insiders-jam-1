﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corpse : MonoBehaviour {

    public GameObject DeathVfxPrefab;

    private void Start() {
        //this.GetComponent<Animator>().Play("Die");
    }
    public virtual void SpawnDeathVFX() {
        if(DeathVfxPrefab != null) {
            Instantiate(DeathVfxPrefab, this.transform.position, this.transform.rotation);
        }
    }
    public void Kill() {
        Destroy(this.gameObject);
    }
    public void RestartGame() {
        GM.instance.FadeAndRestartGame();
    }
}
