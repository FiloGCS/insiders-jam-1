﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIHealing : MonoBehaviour {
    [Header("UI References")]
    [SerializeField]
    public TextMeshProUGUI healingText;

    private int healingValue;

    public int HealingValue {
        get => healingValue;
        set {
            healingValue = value;
            healingText.text = "+" + value.ToString();
        }
    }
}
