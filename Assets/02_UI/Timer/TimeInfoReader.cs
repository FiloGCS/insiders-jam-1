﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimeInfoReader : MonoBehaviour {

    public TextMeshProUGUI timeText;

    private void Start() {
        timeText.text = GM.instance.GetTimeAsString();
    }

}
