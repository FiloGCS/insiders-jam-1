﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GM : MonoBehaviour {

    public static GM instance;

    [Header("UI")]
    public Canvas canvas;
    public UIHealing uiHealingPrefab;
    public UIDamage uiDamagePrefab;
    public UIInteract uiInteractPrefab;

    [Header("Player")]
    public GameObject playerPrefab;
    public Player player;


    [Header("Floors")]
    public int floor = 0;
    public List<string> floorScenes;

    public bool usedPotions = false;
   
    public System.DateTime startTime;
    public System.TimeSpan lastTime;

    private void Awake() {
        //If GM alrady exists, Destroy this
        if (instance != null) {
            Destroy(this.gameObject);
        } else {
            //I'm the GM
            instance = this;
            DontDestroyOnLoad(this.gameObject);
            //Subscribe to SceneLoaded events
            SceneManager.sceneLoaded -= OnSceneLoaded;
            SceneManager.sceneLoaded += OnSceneLoaded;
            InitializePlayer();
            InitializeUI();
            startTime = System.DateTime.Now;
        }
    }
    public void OnDestroy() {
        if (player) {
            Destroy(player.gameObject);
        }
        if (canvas) {
            Destroy(canvas.gameObject);
        }
    }

    public void InitializePlayer() {
        //Spawn the Player Prefab
        if(player == null) {
            GameObject playerGameObject = Instantiate(playerPrefab);
            GM.instance.player = playerGameObject.GetComponent<Player>();
            DontDestroyOnLoad(player.transform);
        }
    }
    public void InitializeUI() {
        //Get the Canvas
        if(canvas == null) {
            GameObject canvasGameObject = GameObject.Find("Canvas");
            canvas = canvasGameObject.GetComponent<Canvas>();
            DontDestroyOnLoad(canvas.transform);
        }
    }

    public void FadeAndLoadNextFloor() {
        //Start Fade
        canvas.GetComponent<Animator>().Play("FadeToBlack");
        //Wait and load the next floor
        StartCoroutine(WaitAndLoadNextFloor());
    }
    IEnumerator WaitAndLoadNextFloor() {
        yield return new WaitForSeconds(0.5f);
        LoadNextFloor();
    }
    public void LoadNextFloor() {
        floor++;
        if(floor < floorScenes.Count) {
            //There is another floor after this one
            SceneManager.LoadScene(floorScenes[floor]);
        } else {
            //This was the last floor
            //Reset the floor counter
            floor = 0;
            //Calculate this run time
            System.DateTime endTime = System.DateTime.Now;
            lastTime = endTime - startTime;
            //Delete the player and canvas
            Destroy(player.gameObject); 
            Destroy(canvas.gameObject);
            //Load the fitting victory scene
            if (usedPotions) {
                this.GetComponent<ScoreManager>().PostTime(lastTime, false);
                SceneManager.LoadScene("Victory");
            } else {
                this.GetComponent<ScoreManager>().PostTime(lastTime, true);
                SceneManager.LoadScene("VictoryTrueEnding");
            }
        }
    }

    public void FadeAndRestartGame() {
        //Start Fade
        canvas.GetComponent<Animator>().Play("FadeToBlack");
        //Wait and load the next floor
        StartCoroutine(WaitAndRestartGame());
    }
    IEnumerator WaitAndRestartGame() {
        yield return new WaitForSeconds(0.5f);
        RestartGame();
    }
    public void RestartGame() {
        //Reset the floor counter
        floor = 0;
        //Delete the player and canvas
        if (player) {
            Destroy(player.gameObject);
        }
        if (canvas) {
            Destroy(canvas.gameObject);
        }
        //Load the run failed scene
        SceneManager.LoadScene("RetryMenu");
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        switch (SceneManager.GetActiveScene().name) {
            case "TutorialScene":
                InitializePlayer();
                InitializeUI();
                startTime = System.DateTime.Now;
                break;
            default:
                break;
        }

        if (canvas && canvas.GetComponent<Animator>()) {
            canvas.GetComponent<Animator>().Play("FadeFromBlack");
        }

        if (instance.player != null) {
            instance.player.MoveToSpawn();
        }
    }

    public string GetTimeAsString() {
        return (lastTime.Minutes + ":" + (lastTime.Seconds % 60) + "." + (lastTime.Milliseconds % 1000) / 10);
    }
}
