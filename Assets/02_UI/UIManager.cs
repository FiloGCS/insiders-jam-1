﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    public static UIManager instance;

    public Canvas canvas;

    //public static UIHealing uiHealingPrefab;
    public UIDamage uiDamagePrefab;
    public UIInteract uiInteractPrefab;
    
    public void Awake() {
        if(instance == null) {
            instance = this;
        } else {
            Destroy(this.gameObject);
        }
    }
}
