﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;

public class ScoreManager : MonoBehaviour {

    private string URL_POST_SCORE = "https://www.gcou.es/game/pachango/PostTime.php";

    public string username = "anonymous";

    private void Start() {
    }

    public void PostTime(TimeSpan t, bool hardcore) {
        if(t.Hours < 1) {
            int totalMilliseconds = t.Milliseconds + t.Seconds * 1000 + t.Minutes * 60000;
            StartCoroutine(PostTimeCoroutine(totalMilliseconds, hardcore));
        }
    }

    IEnumerator PostTimeCoroutine(int milliseconds, bool hardcore) {
        WWWForm form = new WWWForm();
        form.AddField("name", username);
        form.AddField("time", milliseconds);
        form.AddField("version", "alpha");
        string mode = hardcore ? "hc" : "nm";
        form.AddField("mode", mode);
        using (UnityWebRequest request = UnityWebRequest.Post(URL_POST_SCORE, form)) {
            yield return request.SendWebRequest();
            if(request.isNetworkError || request.isHttpError) {
                Debug.Log(request.error);
            } else {
                Debug.Log(request.downloadHandler.text);
            }
        }
    }

}
