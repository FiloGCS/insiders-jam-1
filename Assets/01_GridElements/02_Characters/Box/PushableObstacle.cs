﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushableObstacle : Character {

    public Vector2Int pushableSliding = Vector2Int.zero;

    override public void ReceiveDamage(int dmg, GridElement source) {
        //TODO Ñapeado, hay que meterle clamps a la dirección
        Vector2Int direction = this.tile - source.tile;
        pushableSliding = direction;
    }

    public override void DoTurn() {
        base.DoTurn();
        if (pushableSliding != Vector2Int.zero) {
            TargetTile(this.tile + pushableSliding);
        }
    }

    public override bool TargetTile(Vector2Int targetTile) {

        //Find the elements at the target tile
        List<GridElement> tileElements = TileGrid.GetElementsAtTile(targetTile);

        //Check if I will slide into someone in the next tile
        Character target = CheckAttack(tileElements);
        if (target) {
            //If I crash into someone, so be it
            Attack(target);
            Die();
        }

        //Check if I can slide into the next tile (is there floor?)
        GridElement floor = CheckMove(tileElements);
        if (floor) {
            //If I can keep sliding, do so
            this.tile = floor.tile;
        } else {
            //If I can't keep sliding, crash
            Die();
        }
        return true;
    }
    public override Character CheckAttack(List<GridElement> tileElements) {
        //Check for GridElements in the Character layer
        GridElement target = TileGrid.FindElementByLayer(tileElements, GridLayer.Characters);

        if (target != null && target is Character) {
            //I can attack the target
            return (Character)target;
        }
        //There is no target to attack
        return null;
    }
}
