﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIDamage : MonoBehaviour {
    [Header("UI References")]
    [SerializeField]
    public TextMeshProUGUI damageText;

    private int damageValue;

    public int DamageValue {
        get => damageValue;
        set {
            damageValue = value;
            damageText.text = "-" + value.ToString();
        }
    }
}
