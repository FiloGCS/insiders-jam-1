﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : GridElement { 
    public SpriteRenderer topRenderer;
    public SpriteRenderer bottomRenderer;
    public Sprite[] topSprites;
    public Sprite[] bottomSprites;

    public void SetWallConnections(int index) {
        topRenderer.sprite = topSprites[index];
        bottomRenderer.sprite = bottomSprites[index];
    }

}
