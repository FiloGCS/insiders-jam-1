﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : Item {

    public GameObject PotionCorpse;
    public List<Potion> linkedItems;

    public override void Interact() {
        base.Interact();
        GM.instance.usedPotions = true;
        foreach(Potion item in linkedItems) {
            item.Waste();
        }
        this.Waste();
    }

    public void Waste() {
        if (PotionCorpse) {
            GameObject corpse = Instantiate(PotionCorpse, this.transform.position, this.transform.rotation);
            corpse.GetComponentInChildren<SpriteRenderer>().sortingOrder = (int)(-this.transform.position.z * 100);
        }
        Destroy(this.gameObject);
    }

}
