﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMogu : Character {

    public int visionRange = 6;


    public override void WarnTurn() {
        base.WarnTurn();
        //Only warn next turn if this enemy has found the player
        //if (foundPlayer) {
        //    if (ac) {
        //        ac.speed = 1f / TileGrid.instance.warningTime;
        //        ac.Play("Warn");
        //    }
        //}
    }
    public override void DoTurn() {

        //If I'm sliding keep sliding
        if (this.sliding != Vector2Int.zero) {
            TargetTile(this.tile + sliding);
        }
        if(sliding == Vector2Int.zero) {
            if (!detectedPlayer) {
                //When I still haven't found the player
                //Check if I can see the player
                if (Vector2Int.Distance(this.tile, TileGrid.instance.player.tile) < 7) {
                    if (TileGrid.CanSeeThroughLine(this.tile, TileGrid.instance.player.tile)) {
                        //We found the player
                        detectedPlayer = true;
                        if (ac) {
                            ac.Play("DetectPlayer");
                        }
                    }
                }
            } else {
                //When I can see the player
                if (TileGrid.isNeighbour4(TileGrid.instance.player.tile, this.tile)) {
                    //If I'm next to the player, attack him
                    TargetTile(TileGrid.instance.player.tile);
                } else {
                    //If I'm not next to the player
                    if (Random.Range(0f, 1f) > 0.95f) {
                        //In som random cases just move to a random neighbour
                        MoveToRandomTileFromList(TileGrid.getNeighbours4(this.tile));
                        //Without breaking eye contact
                        //Look(TileGrid.instance.player.tile);
                    } else {
                        //But usually walk towards him
                        Vector2Int playerDirection = TileGrid.instance.player.tile - this.tile;
                        MoveTowardsPlayerDumb(playerDirection);
                    }
                }
            }
        }
        base.DoTurn();


    }
    public override void UpdatePlayerVisibility() {
        //Color by distance to the player
        if (
            Vector2Int.Distance(TileGrid.instance.player.tile, this.tile) < 7.0f &&
            TileGrid.CanSeeThroughLine(TileGrid.instance.player.tile, this.tile, false)
        ) {
            //Activate my UI
            healthUI.gameObject.SetActive(true);
            //Colorize me depending on the distance
            float lambda = 0;
            lambda = 1 - (float)(Vector2Int.Distance(TileGrid.instance.player.tile, this.tile)) / 6.0f;
            lambda = Mathf.Clamp01(lambda);
            visibilityTint = Color.Lerp(new Color(0.55f, 0.5f, 0.5f), Color.white, lambda);
        } else {
            visibilityTint = new Color(0, 0, 0, 0);
            //Hide my UI
            healthUI.gameObject.SetActive(false);
        }
    }
    public override void Move(Vector2Int targetTile) {
        base.Move(targetTile);
    }
    public override void Look(Vector2Int targetTile) {
        if (ac) {
            Vector2Int dir = targetTile - this.tile;
            if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y)) {
                if (dir.x > 0) {
                    //Right
                    print("Look Right");
                    ac.Play("IdleRight");
                } else {
                    //Left
                    print("Look Left");
                    ac.Play("IdleLeft");
                }
            } else {
                if (dir.y > 0) {
                    //Up
                    print("Look Up");
                    ac.Play("IdleUp");
                } else {
                    //Down
                    print("Look Down");
                    ac.Play("IdleDown");
                }
            }
        }
        base.Look(targetTile);
    }
    public void MoveTowardsPlayerDumb(Vector2Int direction) {
        Vector2Int direction1 = Vector2Int.zero;
        Vector2Int direction2 = Vector2Int.zero;

        //Depending on a coinflip
        if (Random.Range(0f, 1f) > 0.5f) {
            //Approach the player horizontally first
            if (direction.x != 0) {
                direction1 = new Vector2Int(direction.x / Mathf.Abs(direction.x), 0);
            }
            if (direction.y != 0) {
                direction2 = new Vector2Int(0, direction.y / Mathf.Abs(direction.y));
            }
        } else {
            //Approach the player vertically first
            if (direction.x != 0) {
                direction2 = new Vector2Int(direction.x / Mathf.Abs(direction.x), 0);
            }
            if (direction.y != 0) {
                direction1 = new Vector2Int(0, direction.y / Mathf.Abs(direction.y));
            }
        }

        if (direction1 == Vector2Int.zero || !TargetTile(this.tile + direction1)) {
            if (direction2 == Vector2Int.zero || !TargetTile(this.tile + direction2)) {
                MoveToRandomTileFromList(TileGrid.getNeighbours4(this.tile));
            }
        }
    }

    public void MoveToRandomTileFromList(List<Vector2Int> tiles) {
        while (tiles.Count > 0) {
            Vector2Int randomNeighbour = tiles[Random.Range(0, tiles.Count)];
            if (TargetTile(randomNeighbour)) {
                //We moved, nice
                break;
            }
            tiles.Remove(randomNeighbour);
        }
    }

}
