﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIHealth : MonoBehaviour {
    [Header("UI References")]
    [SerializeField]
    public TextMeshPro healthText;

    private int healthValue;

    public int HealthValue {
        get => healthValue;
        set {
            healthValue = value;
            healthText.text = value.ToString() + " hp";
        }
    }
}
