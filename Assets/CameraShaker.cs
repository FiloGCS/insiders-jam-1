﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaker : MonoBehaviour {    
    public void Shake() {
        this.GetComponent<Animator>().Play("Shake");
    }
    public void ShortShake() {
        this.GetComponent<Animator>().Play("ShortShake");
    }
    public void PlayerAttack() {
        this.GetComponent<Animator>().Play("PlayerAttack");
    }
    public void StopShaking() {
        this.GetComponent<Animator>().Play("Idle");
    }
}
