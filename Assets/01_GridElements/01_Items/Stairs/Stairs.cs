﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stairs : Item {
    
    public override void Interact() {
        base.Interact();
        print("Let's go to the next floor");
        GM.instance.FadeAndLoadNextFloor();
        Destroy(this);
    }
}
