﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : GridElement {


    [Header("Parameters")]
    public bool interactable = true;
    public bool oneUse = true;
    protected bool used = false;
    public string interactionString = "Interact";
    public Sprite interactionSprite;
    public Vector3 uiInteractPositionOffset = Vector3.zero;

    private UIInteract myUIInteract;

    override public void DoTurn() {
        base.DoTurn();
    }

    public override void Start() {
        base.Start();
        if (interactable) {
            myUIInteract = Instantiate(GM.instance.uiInteractPrefab, GM.instance.canvas.transform);
            myUIInteract.transform.position = Camera.main.WorldToScreenPoint(this.transform.position + uiInteractPositionOffset);
            myUIInteract.InteractString = interactionString;
            myUIInteract.InteractSprite = interactionSprite;
            myUIInteract.Show(false);
        }
    }

    public override void Update() {
        base.Update();
        if (interactable) {
            //Check if the player is in my tile
            if (TileGrid.instance.player && TileGrid.instance.player.tile == this.tile) {
                //Show my interaction UI
                myUIInteract.Show(true);
            } else {
                //Hide my interaction UI
                myUIInteract.Show(false);
            }
            myUIInteract.transform.position = Camera.main.WorldToScreenPoint(this.transform.position + uiInteractPositionOffset);
        }
    }

    public override void UpdatePlayerVisibility() {
        //Color by distance to the player
        if (
            Vector2Int.Distance(TileGrid.instance.player.tile, this.tile) < 7.0f &&
            TileGrid.CanSeeThroughLine(TileGrid.instance.player.tile, this.tile, false)
        ) {
            float lambda = 0;
            lambda = 1 - (float)(Vector2Int.Distance(TileGrid.instance.player.tile, this.tile)) / 7.0f;
            lambda = Mathf.Clamp01(lambda);
            visibilityTint = Color.Lerp(new Color(0.55f, 0.5f, 0.5f), Color.white, lambda);
        } else {
            visibilityTint = new Color(0,0,0,0);
        }
    }

    public virtual void Interact() {
        if (!interactable) {
            return;
        }
    }
    
    override public void OnDestroy() {
        if (myUIInteract) {
            Destroy(myUIInteract.gameObject);
        }
        base.OnDestroy();
    }

}
