﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : Character {
    
    //DEBUG
    public GameObject walkingMarkerPrefab;
    private List<GameObject> walkingMarkers;

    [Header("Audio")]
    public AudioClip stepAudio;
    public AudioClip attackAudio;
    public AudioClip killEnemyAudio;
    public AudioClip dieAudio;
    public AudioClip useAudio;

    override public void Awake() {
        //Player is a singleton
        if (GM.instance.player != null) {
            Destroy(this.gameObject);
        } else {
            GM.instance.player = this;
            base.Awake();
            walkingMarkers = new List<GameObject>();
        }
    }

    public override void Start() {
        this.MoveToSpawn();
        base.Start();
        Shader.SetGlobalVector("PlayerPos", TileGrid.gridToWorld(this.tile));
    }

    override public void Update() {
        base.Update();

        //Update my position in all shaders
        Shader.SetGlobalVector("PlayerSpritePos", this.transform.position);

        //TODO ñapa
        if (!TileGrid.instance.isBusy()) {
            
            //If I'm sliding
            if(this.sliding != Vector2Int.zero) {
                //Check if I can keep sliding
                Vector2Int nextTile = this.tile + sliding;
                List<GridElement> elements = TileGrid.GetElementsAtTile(nextTile);
                //If I can move to the next tile and there is noone to attack there
                if (!CheckAttack(elements) && CheckMove(elements)) {
                    TargetTile(nextTile);
                } else {
                    //Stop Sliding
                    sliding = Vector2Int.zero;
                }
            }

            //If I'm not sliding I can do anything
            if(sliding == Vector2Int.zero) {
                //MOVEMENT
                if (Keyboard.current.aKey.isPressed || Keyboard.current.leftArrowKey.isPressed) {
                    //LEFT
                    TargetTileIfNotWasting(this.tile + new Vector2Int(-1, 0));
                } else if (Keyboard.current.dKey.isPressed || Keyboard.current.rightArrowKey.isPressed) {
                    //RIGHT
                    TargetTileIfNotWasting(this.tile + new Vector2Int(1, 0));
                } else if (Keyboard.current.wKey.isPressed || Keyboard.current.upArrowKey.isPressed) {
                    //UP
                    TargetTileIfNotWasting(this.tile + new Vector2Int(0, 1));
                } else if (Keyboard.current.sKey.isPressed || Keyboard.current.downArrowKey.isPressed) {
                    //DOWN
                    TargetTileIfNotWasting(this.tile + new Vector2Int(0, -1));
                }

                //ITEM INTERACTION
                if (Keyboard.current.eKey.wasPressedThisFrame || Keyboard.current.spaceKey.wasPressedThisFrame) {
                    InteractWithItem(this.tile);
                    DoPlayerTurn();
                }
            }
        }
    }
    override public void OnDestroy() {
        //Clear walking markers
        if(walkingMarkers != null) {
            foreach (GameObject marker in walkingMarkers) {
                Destroy(marker);
            }
        }
    }
    public override void WarnTurn() {
        base.WarnTurn();
        if (TileGrid.PlayerOverCrackingTile() || TileGrid.ExistsEnemyInPlayerMeleeRange()) {
            Camera.main.GetComponent<CameraShaker>().Shake();
        }
    }
    public override void Move(Vector2Int targetTile) {
        GetComponent<AudioSource>().PlayOneShot(stepAudio,0.6f);
        Vector2Int delta = targetTile - this.tile;
        base.Move(targetTile);
        Shader.SetGlobalVector("PlayerPos", TileGrid.gridToWorld(this.tile));
        TileGrid.DisplaceVisionTexture(delta.x, delta.y);
    }
    public override void Attack(Character target) {
        base.Attack(target);
        //Did I kill my target
        if(target.Health <= 0) {
            Camera.main.GetComponent<CameraShaker>().PlayerAttack();
            //GameObject FacePanel = GameObject.Find("FacePanel");
            //if (FacePanel) {
            //    FacePanel.GetComponent<Animator>().Play("LaserEyes");
            //}
            GetComponent<AudioSource>().PlayOneShot(killEnemyAudio);
        } else {
            GetComponent<AudioSource>().PlayOneShot(attackAudio);
        }
    }
    public override void Die() {
        AudioSource.PlayClipAtPoint(dieAudio, this.transform.position);
        base.Die();
    }
    public override void ReceiveDamage(int dmg, GridElement source) {
        GetComponent<AudioSource>().PlayOneShot(attackAudio);
        Camera.main.GetComponent<CameraShaker>().ShortShake();
        base.ReceiveDamage(dmg, source);
    }
    public void TargetTileIfNotWasting(Vector2Int targetTile) {
        if (CanDoSomething(targetTile)) {
            TargetTile(targetTile);
        }
    }
    public override bool TargetTile(Vector2Int targetTile) {
        bool result = base.TargetTile(targetTile);
        DoPlayerTurn();
        return result;
    }
    public void DoPlayerTurn() {
        TileGrid.DoTurns();
        //Stop Camera Shake
        Camera.main.GetComponent<CameraShaker>().StopShaking();
    }
    public void InteractWithItem(Vector2Int targetTile) {
        GridElement item = TileGrid.GetElementAtTile(targetTile, GridLayer.Items);
        if (item!=null  && (Item)item) {
            item.GetComponent<Item>().Interact();
            GetComponent<AudioSource>().PlayOneShot(useAudio);
        }
    }

    public void MoveToSpawn() {
        //Find the player spawner in this level
        PlayerSpawn spawn = FindObjectOfType<PlayerSpawn>();
        //Move to that tile and position
        this.transform.position = spawn.transform.position;

        SnapToWorld();
        //Tell the camraman to move with me
        FindObjectOfType<Cameraman>().transform.position = this.transform.position;
        //Tell the TileGrid I'm here if he doesn't know about me
        TileGrid.AddElement(this);

        Shader.SetGlobalVector("PlayerPos", TileGrid.gridToWorld(this.tile));
    }
  
    //TODO ÑAPA
    private void UpdateWalkingMarkers() {
        //Clear previous markers
        foreach (GameObject marker in walkingMarkers) {
            Destroy(marker);
        }
        //For each neighbour
        foreach (Vector2Int tile in TileGrid.getNeighbours4(this.tile)) {
            //If I can walk to it
            if (TileGrid.CanWalk(tile)) {
                GameObject marker = Instantiate(walkingMarkerPrefab, TileGrid.gridToWorld(tile), Quaternion.identity);
                walkingMarkers.Add(marker);
            }
        }
    }
    
}
