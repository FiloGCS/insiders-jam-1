﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionHp : Potion {
    public int healing = 5;

    public override void Interact() {
        base.Interact();
        GM.instance.player.ReceiveHealing(healing);
        Destroy(this.gameObject);
    }
}
