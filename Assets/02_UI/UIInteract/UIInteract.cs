﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIInteract : MonoBehaviour {

    [Header("UI References")]
    [SerializeField]
    public TextMeshProUGUI interactText;
    [SerializeField]
    public Image interactSpriteRenderer;

    private void Start() {
        this.transform.SetSiblingIndex(0);
    }

    private Sprite interactSprite;
    private string interactString;
    
    public Sprite InteractSprite {
        get => interactSprite;
        set {
            interactSprite = value;
            interactSpriteRenderer.sprite = interactSprite;
        }
    }
    public string InteractString {
        get => interactString;
        set {
            interactString = value;
            interactText.text = interactString;
        }
    }
    
    public void Show(bool show) {
        if (show) {
            this.GetComponent<Animator>().Play("Show");
        } else{
            this.GetComponent<Animator>().Play("Hide");
        }
    }
}
