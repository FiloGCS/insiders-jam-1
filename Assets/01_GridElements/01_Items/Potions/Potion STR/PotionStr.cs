﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionStr : Potion {
    public int strength = 2;

    public override void Interact() {
        base.Interact();
        GM.instance.player.Strength+=strength;
        Destroy(this.gameObject);
    }
}
