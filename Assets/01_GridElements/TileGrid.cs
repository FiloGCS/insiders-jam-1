﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GridLayer { Floor, Items, Wall, Characters, Count }

public class TileGrid : MonoBehaviour {
    //Singleton
    public static TileGrid instance;

    //References
    [Header("UI References")]
    public GameObject TilePrefab;

    [Header("Vision")]
    public Texture2D visionTex;
    public int[,] visionArray;
    public int visionRadius;
    public AnimationCurve visionCurve = AnimationCurve.Linear(0,1,1,0.3f);
    public float fogValue = 0.15f;
    public FilterMode filterMode = FilterMode.Bilinear;

    //Parameters
    [Header("Parameters")]
    public Vector3 center = Vector3.zero;
    [SerializeField]
    public static float tileSize = 1.0f;
    [SerializeField]
    public static float layerDepth = 0.2f;
    public int size = 10;
    public float busyTime = 0.25f;
    public float patienceTime = 2f;
    public float warningTime = 1f;

    public static float[] layerOffsets = { 0f, 2f, 1f, 3f, 5f };

    //State
    private Dictionary<Vector2Int, GridElement>[] elements;
    public Player player;
    public float busy = 0;
    public float patience = 1;

    //Methods
    private void Awake() {
        //Singleton
        if (instance != null) {
            Destroy(this);
        } else {
            instance = this;
        }

        //Initialize layers
        elements = new Dictionary<Vector2Int, GridElement>[(int)GridLayer.Count];
        for (int i = 0; i < elements.Length; i++) {
            elements[i] = new Dictionary<Vector2Int, GridElement>();
        }
        //Initialize vision
        int n = visionRadius * 2 + 1;
        visionArray = new int[n, n];
        visionTex = new Texture2D(n, n);
        for (int i = 0; i < instance.visionTex.width; i++) {
            for (int j = 0; j < instance.visionTex.height; j++) {
                visionTex.SetPixel(i, j,Color.black);
            }
        }
        visionTex.Apply();
        visionTex.wrapMode = TextureWrapMode.Clamp;
        visionTex.filterMode = filterMode;
        Shader.SetGlobalTexture("VisionTex", visionTex);
    }
    private void Start() {
        player = FindObjectOfType<Player>();
    }
    private void Update() {
        //visionTex.filterMode = filterMode;
        //Reduce busy
        busy = Mathf.Clamp(busy - Time.deltaTime, 0, Mathf.Infinity);
        //Reduce patience
        if (patience > 0) {
            float previousPatience = patience;
            patience = Mathf.Clamp(patience - Time.deltaTime, 0, Mathf.Infinity);
            if (patience == 0) {
                //Do next turn
                DoTurns();
            } else if (previousPatience > warningTime && patience < warningTime) {
                //Tell every single GridElement to warn
                WarnTurns();
            } else {
            }
        }
        if(GM.instance && GM.instance.player) {
            UpdateVisionTex();
        }
    }

    //GridElement management
    public static bool AddElement(GridElement newElement) {
        GridLayer layer = newElement.layer;
        //If this tile in this layer is empty
        if (instance.elements[(int)layer].ContainsKey(newElement.tile)) {
            //there is already a GridElement in this layer and tile
            return false;
        } else {
            //This layer and tile combination is free
            instance.elements[(int)layer][newElement.tile] = newElement;
            return true;
        }
    }
    public static bool RemoveElement(GridElement e) {
        GridLayer layer = e.layer;
        return instance.elements[(int)layer].Remove(e.tile);
    }
    public static bool MoveElement(GridElement e, Vector2Int newTile, bool teleport) {
        //Remove the element from the dictionaries
        if (!RemoveElement(e)) {
            return false;
        }
        //Update the element tile
        e.tile = newTile;
        if (teleport) {
            e.SnapToGrid();
        }
        //Add the modified element to the dictionaries
        if (!AddElement(e)) {
            return false;
        }
        return true;
    }
    public static bool MoveElement(GridElement e, Vector2Int newTile) {
        return MoveElement(e, newTile, false);
    }
    public static GridElement GetElementAtTile(Vector2Int tile, GridLayer layer) {
        if (instance.elements[(int)layer].ContainsKey(tile)) {
            return instance.elements[(int)layer][tile];
        } else {
            return null;
        }
    }
    public static List<GridElement> GetElementsAtTile(Vector2Int tile) {

        List<GridElement> result = new List<GridElement>();

        foreach (Dictionary<Vector2Int, GridElement> layer in instance.elements) {
            if (layer.ContainsKey(tile)) {
                result.Add(layer[tile]);
            }
        }
        return result;
    }
    public static GridElement FindElementByLayer(List<GridElement> elements, GridLayer layer) {
        foreach (GridElement element in elements) {
            if (element.layer == layer) {
                return element;
            }
        }
        return null;
    }
    public static List<GridElement> GetElementsFromLayer(GridLayer layer) {
        List<GridElement> result = new List<GridElement>();
        foreach (KeyValuePair<Vector2Int, GridElement> pair in instance.elements[(int)layer]) {
            result.Add(pair.Value);
        }
        return result;
    }
    public static List<GridElement> GetAllElements() {
        List<GridElement> result = new List<GridElement>();
        foreach (Dictionary<Vector2Int, GridElement> layer in instance.elements) {
            //for each value pair in the layer
            foreach (KeyValuePair<Vector2Int, GridElement> pair in layer) {
                result.Add(pair.Value);
            }
        }
        return result;
    }
    public static bool CanWalk(Vector2Int tile) {
        //If there is no floor at that position
        if (GetElementAtTile(tile, GridLayer.Floor) == null) {
            return false;
        }
        return true;
    }
    public static bool ExistCharacterThatDetectedPlayer() {
        bool exists = false;
        foreach (GridElement character in GetElementsFromLayer(GridLayer.Characters)) {
            if (character != null && (Character)character) {
                if (((Character)character).detectedPlayer) {
                    exists = true;
                    break;
                }
            }
        }
        return exists;
    }
    public static bool ExistsEnemyInPlayerMeleeRange() {
        bool exists = false;
        foreach (GridElement character in GetElementsFromLayer(GridLayer.Characters)) {
            if (character != null && (Character)character && character != instance.player) {
                if (TileGrid.isNeighbour4(character.tile, instance.player.tile)) {
                    exists = true;
                    break;
                }
            }
        }
        return exists;
    }
    public static List<Character> GetCharactersThatDetectedPlayer() {
        List<Character> result = new List<Character>();
        foreach (GridElement character in GetElementsFromLayer(GridLayer.Characters)) {
            if (character != null && (Character)character) {
                if (((Character)character).detectedPlayer) {
                    result.Add((Character)character);
                }
            }
        }
        return result;
    }

    //Turns
    public static void WarnTurns() {
        //For each layer
        foreach (Dictionary<Vector2Int, GridElement> layer in instance.elements) {
            //for each value pair in the layer
            foreach (KeyValuePair<Vector2Int, GridElement> pair in layer) {
                pair.Value.WarnTurn();
            }
        }
    }
    public static void DoTurns() {
        //Add the busyTime for the character turn
        AddBusyTime();
        //Check if
        if (
            //any enemy is in the player melee range
            ExistsEnemyInPlayerMeleeRange() ||
            //Or the player just stepped into a broken tile
            PlayerOverBrokenTile()
        ) {
            //Wait until the character busyTime ends and do the enemy turn
            instance.StartCoroutine(instance.WaitAndDoTurns(GetAllElements()));
        } else {
            DoTurns(GetAllElements());
            instance.patience = instance.patienceTime + instance.busyTime;
        }
    }
    public IEnumerator WaitAndDoTurns(List<GridElement> elements) {
        AddBusyTime();
        yield return new WaitForSeconds(busyTime);
        DoTurns(elements);
        patience = patienceTime + busyTime;
    }
    public static void DoTurns(List<GridElement> elements) {
        //For each layer
        foreach (GridElement element in elements) {
            if (element != null) {
                element.DoTurn();
            }
        }
    }
    public static void AddBusyTime() {
        instance.busy += instance.busyTime;
    }
    public bool isBusy() {
        return (busy > 0);
    }

    //Grid utils
    public static Vector3 gridToWorld(Vector2Int gridPosition) {
        //TODO test
        Vector3 pos = Vector3.zero;
        pos = (Vector3)(Vector2)gridPosition * tileSize;
        return pos;
    }
    public static Vector3 gridToWorld(Vector2Int gridPosition, GridLayer layer) {
        //TODO test
        Vector3 pos = Vector3.zero;
        pos = (Vector3)(Vector2)gridPosition * tileSize;
        //Add the layer offset
        pos += Vector3.back * layerOffsets[(int)layer] * layerDepth;
        //Add the depth offset
        pos += Vector3.forward * pos.y * 0.1f;
        return pos;
    }
    public static Vector2Int worldToGrid(Vector3 worldPosition) {
        //TODO test
        Vector2 pos = (Vector2)(worldPosition / tileSize);
        return new Vector2Int((int)pos.x, (int)pos.y);
    }
    public static int distance4(Vector2Int a, Vector2Int b) {
        //TODO test
        int xDistance = Mathf.Abs(a.x - b.x);
        int yDistance = Mathf.Abs(a.y - b.y);
        return xDistance + yDistance;
    }
    public static bool isNeighbour4(Vector2Int a, Vector2Int b) {
        //TODO test
        return (distance4(a, b) <= 1);
    }
    public static List<Vector2Int> getNeighbours4(Vector2Int tile) {
        List<Vector2Int> neighbours = new List<Vector2Int>();
        neighbours.Add(new Vector2Int(tile.x + 1, tile.y));
        neighbours.Add(new Vector2Int(tile.x - 1, tile.y));
        neighbours.Add(new Vector2Int(tile.x, tile.y + 1));
        neighbours.Add(new Vector2Int(tile.x, tile.y - 1));
        return neighbours;
    }
    //Grid Line Utils
    private static List<Vector2Int> plotLineLow(int x0, int y0, int x1, int y1) {
        List<Vector2Int> result = new List<Vector2Int>();
        int dx = x1 - x0;
        int dy = y1 - y0;
        int yi = 1;
        if (dy < 0) {
            yi = -1;
            dy = -dy;
        }
        int D = (2 * dy) - dx;
        int y = y0;

        for (int x = x0; x < x1; x++) {
            result.Add(new Vector2Int(x, y));
            if (D > 0) {
                y = y + yi;
                D = D + (2 * (dy - dx));
            } else {
                D = D + 2 * dy;
            }
        }
        return result;
    }
    private static List<Vector2Int> plotLineHigh(int x0, int y0, int x1, int y1) {
        List<Vector2Int> result = new List<Vector2Int>();
        int dx = x1 - x0;
        int dy = y1 - y0;
        int xi = 1;
        if (dx < 0) {
            xi = -1;
            dx = -dx;
        }
        int D = (2 * dx) - dy;
        int x = x0;
        for (int y = y0; y < y1; y++) {
            result.Add(new Vector2Int(x, y));
            if (D > 0) {
                x = x + xi;
                D = D + (2 * (dx - dy));
            } else {
                D = D + 2 * dx;
            }
        }
        return result;
    }
    public static List<Vector2Int> getTilesInLine(Vector2Int origin, Vector2Int target, bool includeTarget) {
        List<Vector2Int> result = new List<Vector2Int>();
        //https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
        int x0 = origin.x;
        int y0 = origin.y;
        int x1 = target.x;
        int y1 = target.y;

        if (Mathf.Abs(y1 - y0) < Mathf.Abs(x1 - x0)) {
            if (x0 > x1) {
                result = plotLineLow(x1, y1, x0, y0);
            } else {
                result = plotLineLow(x0, y0, x1, y1);
            }
        } else {
            if (y0 > y1) {
                result = plotLineHigh(x1, y1, x0, y0);
            } else {
                result = plotLineHigh(x0, y0, x1, y1);
            }
        }
        if (includeTarget) {
            result.Add(target);
        } else {
            result.Remove(target);
        }
        return result;
    }
    public static List<Vector2Int> getTilesInLine(Vector2Int origin, Vector2Int target) {
        return getTilesInLine(origin, target, true);
    }
    //Vision
    public static bool CanSeeThrough(Vector2Int tile) {
        GridElement floor = GetElementAtTile(tile, GridLayer.Floor);
        GridElement wall = GetElementAtTile(tile, GridLayer.Wall);
        if (wall || !floor) {
            return false;
        } else {
            return true;
        }
    }
    public static bool CanSeeThroughLine(Vector2Int origin, Vector2Int target, bool includeTarget) {
        foreach (Vector2Int tile in getTilesInLine(origin, target, includeTarget)) {
            if (!CanSeeThrough(tile)) {
                return false;
            }
        }
        return true;
    }
    public static bool CanSeeThroughLine(Vector2Int origin, Vector2Int target) {
        return CanSeeThroughLine(origin, target, true);
    }
    public static bool CanSeeUpToLine(Vector2Int origin, Vector2Int target, bool includeTarget) {
        List<Vector2Int> tiles = getTilesInLine(origin, target, includeTarget);
        if (tiles.Count > 0) {
            tiles.RemoveAt(tiles.Count - 1);
        }
        foreach (Vector2Int tile in tiles) {
            if (!CanSeeThrough(tile)) {
                return false;
            }
        }
        return true;
    }

    public static int UpdateVisionTexPixel(Vector2Int pixel) {
        int result = 0;
        Vector2Int tile = GM.instance.player.tile + pixel - new Vector2Int(instance.visionRadius, instance.visionRadius);
        int distance = distance4(GM.instance.player.tile, tile);
        float pixelValue;
        if ( distance < instance.visionRadius) {
            //If the tile is within the player vision range
            if (CanSeeThroughLine(instance.player.tile, tile, true)) {
                //If the tile is in the player's line of sight
                pixelValue = instance.visionCurve.Evaluate((float)distance / (float)instance.visionRadius);
            } else {
                pixelValue = instance.fogValue; 
            }
            //Update this pixel in the vision Texture
            Color target = new Color(pixelValue, pixelValue, pixelValue);
            Color previous = instance.visionTex.GetPixel(instance.visionTex.width - pixel.x, instance.visionTex.height - pixel.y);
            Color c = Color.Lerp(previous, target, Time.deltaTime*5);
            instance.visionTex.SetPixel(instance.visionTex.width - pixel.x,instance.visionTex.height - pixel.y, c);
        }
        return result;
    }
    public static void UpdateVisionTex() {
        //Start painting 
        Vector2Int offset = new Vector2Int(instance.visionRadius, instance.visionRadius);
        for (int i=0; i < instance.visionTex.width; i++) {
            for(int j = 0; j < instance.visionTex.height; j++) {
                Vector2Int pixel = new Vector2Int(i, j);
                UpdateVisionTexPixel(pixel);
            }
        }
        instance.visionTex.Apply();
    }
    public static void DisplaceVisionTexture(int x, int y) {
        Texture2D oldTexture = new Texture2D(instance.visionTex.width,instance.visionTex.height);
        Graphics.CopyTexture(instance.visionTex, oldTexture);
        for (int i = 0; i < instance.visionTex.width; i++) {
            for (int j = 0; j < instance.visionTex.height; j++) {
                Color c = oldTexture.GetPixel(i - x, j - y);
                instance.visionTex.SetPixel(i, j, c);
            }
        }
        instance.visionTex.Apply();
    }

    //Player
    public static bool PlayerOverBrokenTile() {
        GridElement tile = GetElementAtTile(instance.player.tile, GridLayer.Floor);
        if (tile is CrackingTile && ((CrackingTile)tile).integrity ==0){
            return true;
        } else {
            return false;
        }
    }
    public static bool PlayerOverCrackingTile() {
        GridElement tile = GetElementAtTile(instance.player.tile, GridLayer.Floor);
        if (tile is CrackingTile) {
            return true;
        } else {
            return false;
        }
    }

    public static void UpdatePlayerVisibility() {
        if (TileGrid.instance.player != null) {
            foreach (GridElement element in GetAllElements()) {
                element.UpdatePlayerVisibility();
            }
        }
    }
}
