﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridElement : MonoBehaviour {

    [Header("Parameters")]
    public GridLayer layer;


    public Vector2Int tile;
    public bool actionDelayed = false;
    private List<string> tags;

    protected Color visibilityTint = Color.white;
    protected float visibilityTintLerpSpeed = 10f;
    protected float movementLerpSpeed = 7f;

    public virtual void Awake() {
    }
    public virtual void Start() {
        SnapToWorld();
        TileGrid.AddElement(this);
    }
    public virtual void Update() {
        this.transform.position = Vector3.Lerp(this.transform.position, TileGrid.gridToWorld(tile,layer), movementLerpSpeed * Time.deltaTime);

        LerpSpriteRenderersTowardsVisibilityTint();
        UpdateSortingOrder();
    }
    public virtual void OnDestroy() {
        TileGrid.RemoveElement(this);
    }

    public virtual void WarnTurn() {

    }
    public virtual void DoTurn() {
        UpdatePlayerVisibility();
    }
    
    public virtual void UpdatePlayerVisibility() {
        //Color by distance to the player
        //if (
        //    Vector2Int.Distance(TileGrid.instance.player.tile, this.tile) < 7.0f &&
        //    TileGrid.CanSeeThroughLine(TileGrid.instance.player.tile, this.tile, false)
        //) {
        //    float lambda = 0;
        //    lambda = 1 - (float)(Vector2Int.Distance(TileGrid.instance.player.tile, this.tile)) / 7.0f;
        //    lambda = Mathf.Clamp01(lambda);
        //    visibilityTint = Color.Lerp(new Color(0.55f, 0.5f, 0.5f), Color.white, lambda);
        //} else {
        //    visibilityTint = new Color(0.35f, 0.35f, 0.35f);
        //}
    }

    public virtual void LerpSpriteRenderersTowardsVisibilityTint() {
        foreach(SpriteRenderer r in GetComponentsInChildren<SpriteRenderer>()) {
            r.color = Color.Lerp(r.color, visibilityTint, visibilityTintLerpSpeed * Time.deltaTime);
        }
    }
    
    public void SnapToGrid() {
        this.transform.position = TileGrid.gridToWorld(tile, this.layer);
    }
    public void SnapToWorld() {
        this.tile = TileGrid.worldToGrid(this.transform.position);
        this.transform.position = TileGrid.gridToWorld(tile, this.layer);
    }
    public void UpdateSortingOrder() {
        foreach (Renderer r in GetComponentsInChildren<Renderer>()) {
            r.sortingOrder = (int)(-this.transform.position.z * 100);
        }
    }
    public void SetGridPosition(Vector2Int gridPos) {
        this.tile = gridPos;
        this.transform.position = TileGrid.gridToWorld(tile, this.layer);
    }
    
}
