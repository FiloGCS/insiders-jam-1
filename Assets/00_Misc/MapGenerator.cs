﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class MapGenerator : MonoBehaviour {

    public Texture2D mapTexture;

    [Header("Environment elements")]
    public Color emptyColor;
    public GameObject SafeTilePrefab;
    public GameObject CrackingTilePrefab;
    public Color crackingTileColor;
    public GameObject IceTilePrefab;
    public Color iceTileColor;
    public GameObject WallPrefab;
    public Color wallColor;
    [Header("Item Elements")]
    public GameObject StairsPrefab;
    public Color stairsColor;
    public GameObject PotionPrefab;
    public Color potionColor;

    [Header("Character elements")]
    public GameObject EnemyPrefab;
    public Color enemyColor;
    public GameObject PlayerPrefab;
    public Color playerColor;

    public bool LOAD_MAP = false;

    private void Awake() {
        LOAD_MAP = false;
        GameObject loadedMap = GameObject.Find("Loaded Map");
    }

    private void Update() {
        if (LOAD_MAP) {
            LOAD_MAP = false;
            GenerateMap();
        }
    }

    public void GenerateMap(Texture2D source) {
        mapTexture = source;
        GenerateMap();
    }

    public void GenerateMap() {
        int n = mapTexture.width;
        int m = mapTexture.height;

        //Create the map parent
        GameObject mapParent = new GameObject("Loaded Map");
        mapParent.transform.position = Vector3.zero;

        //For each pixel in the image
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                //FLOOR
                Color c = mapTexture.GetPixel(i, j);
                //Safe Tile is the default
                GameObject selectedPrefab = SafeTilePrefab;
                if (c == emptyColor) {
                    //Empty Tile
                    selectedPrefab = null;
                } else if (c == crackingTileColor) {
                    //Cracking Tile
                    selectedPrefab = CrackingTilePrefab;
                } else if (c == iceTileColor) {
                    //Ice Tile
                    selectedPrefab = IceTilePrefab;
                } else if (c == wallColor) {
                    //Wall means empty floor
                    selectedPrefab = null;
                }
                if (selectedPrefab) {
                    //Spawn the matching environment prefab

                    Object newObject = PrefabUtility.InstantiatePrefab(selectedPrefab, mapParent.transform);
                    if (newObject is GameObject) {
                        ((GameObject)newObject).transform.position = TileGrid.gridToWorld(new Vector2Int(i, j), selectedPrefab.GetComponent<GridElement>().layer);
                    }
                    //GameObject newG = Instantiate(selectedPrefab, TileGrid.gridToWorld(new Vector2Int(i, j), selectedPrefab.GetComponent<GridElement>().layer), Quaternion.identity);
                    //newG.transform.parent = mapParent.transform;
                }
                //WALLS
                if(c == wallColor) {
                    int a = 0;
                    if (mapTexture.GetPixel(i, j+1) != wallColor) {
                        a += 1;
                    }
                    if (mapTexture.GetPixel(i+1, j) != wallColor) {
                        a += 2;
                    }
                    if (mapTexture.GetPixel(i, j-1) != wallColor) {
                        a += 4;
                    }
                    if (mapTexture.GetPixel(i-1, j) != wallColor) {
                        a += 8;
                    }
                    Object newObject = PrefabUtility.InstantiatePrefab(WallPrefab, mapParent.transform);
                    if (newObject is GameObject) {
                        ((GameObject)newObject).transform.position = TileGrid.gridToWorld(new Vector2Int(i, j), WallPrefab.GetComponent<GridElement>().layer);
                        ((GameObject)newObject).GetComponent<Wall>().SetWallConnections(a);
                    }
                }

                //ITEMS
                if (c == stairsColor) {
                    //Spawn stairs
                    Object newObject = PrefabUtility.InstantiatePrefab(StairsPrefab, mapParent.transform);
                    if (newObject is GameObject) {
                        ((GameObject)newObject).transform.position = TileGrid.gridToWorld(new Vector2Int(i, j), StairsPrefab.GetComponent<GridElement>().layer);
                    }
                    //GameObject newG = Instantiate(StairsPrefab, TileGrid.gridToWorld(new Vector2Int(i, j), StairsPrefab.GetComponent<GridElement>().layer), Quaternion.identity);
                    //newG.transform.parent = mapParent.transform;
                } else if (c == potionColor) {
                    Object newObject = PrefabUtility.InstantiatePrefab(PotionPrefab, mapParent.transform);
                    if (newObject is GameObject) {
                        ((GameObject)newObject).transform.position = TileGrid.gridToWorld(new Vector2Int(i, j), PotionPrefab.GetComponent<GridElement>().layer);
                    }
                    //GameObject newG = Instantiate(PotionPrefab, TileGrid.gridToWorld(new Vector2Int(i, j), PotionPrefab.GetComponent<GridElement>().layer), Quaternion.identity);
                    //newG.transform.parent = mapParent.transform;
                }

                //CHARACTERS
                if (c == enemyColor) {
                    Object newObject = PrefabUtility.InstantiatePrefab(EnemyPrefab, mapParent.transform);
                    if (newObject is GameObject) {
                        ((GameObject)newObject).transform.position = TileGrid.gridToWorld(new Vector2Int(i, j), EnemyPrefab.GetComponent<GridElement>().layer);
                    }
                    //Spawn Enemy
                    //GameObject newG = Instantiate(EnemyPrefab, TileGrid.gridToWorld(new Vector2Int(i, j), EnemyPrefab.GetComponent<GridElement>().layer), Quaternion.identity);
                    //newG.transform.parent = mapParent.transform;
                } else if (c == playerColor) {
                    Object newObject = PrefabUtility.InstantiatePrefab(PlayerPrefab, mapParent.transform);
                    if (newObject is GameObject) {
                        ((GameObject)newObject).transform.position = TileGrid.gridToWorld(new Vector2Int(i, j), PlayerPrefab.GetComponent<GridElement>().layer);
                    }
                    //Spawn Player
                    //GameObject newG = Instantiate(PlayerPrefab, TileGrid.gridToWorld(new Vector2Int(i, j), PlayerPrefab.GetComponent<GridElement>().layer), Quaternion.identity);
                    //newG.transform.parent = mapParent.transform;
                }
            }
        }
    }


}

