﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrackingTile : GridElement {
    public SpriteRenderer crackSprite;
    public Sprite[] sprites;
    public int integrity;

    private Character previousCharacter = null;
    private Character currentCharacter = null;

    public override void Start() {
        base.Start();
        //Select a random integrity level
        integrity = Random.Range(1, sprites.Length);
        UpdateSprite();
    }

    public override void DoTurn() {
        base.DoTurn();
        //Update the previousCharacter
        previousCharacter = currentCharacter;
        //Get the current character
        currentCharacter = (Character) TileGrid.GetElementAtTile(this.tile, GridLayer.Characters);
        if(currentCharacter && integrity == 0) {
            currentCharacter.ReceiveDamage(100, this);
        }
        //If the same dude from the last turn is on top of me
        if (currentCharacter != null && currentCharacter == previousCharacter) {
            Crack();
        }
    }

    public void Crack() {
        //Reduce my integrity, clamped
        integrity = Mathf.Clamp(integrity - 1, 0, sprites.Length);
        UpdateSprite();
        //If I break, damage whoever is on top of me
        if (integrity == 0) {
            currentCharacter.ReceiveDamage(100,this);
        }
    }

    public void UpdateSprite() {
        crackSprite.sprite = sprites[integrity];
    }
}
