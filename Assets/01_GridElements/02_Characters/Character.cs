﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Team { Player, Enemies, None, Count}

public class Character : GridElement {

    [Header("UI References")]
    [SerializeField]
    protected UIHealth healthUI;
    [Header("Prefab References")]
    public GameObject myCorpse;

    //Combat
    [Header("Combat Stats")]
    public Team team = Team.None;
    [SerializeField]
    public int maxHealth = 10;
    protected int health = 10;
    [SerializeField]
    protected int strength = 5;

    public bool isBusy = false;
    protected Animator ac;

    //Properties
    public virtual int Health {
        get => health;
        set {
            //Update health value
            health = Mathf.Clamp(value,0,999);
            //If this character has a healthUI, update it
            if (healthUI) {
                healthUI.HealthValue = this.health;
            }
            //If this character has zero health, kill it
            if (health == 0) {
                Die();
            }
        }
    }
    public int Strength {
        get => strength;
        set {
            strength = Mathf.Max(value, 0);
        }
    }

    //Methods
    override public void Awake() {
        base.Awake();
        ac = this.GetComponent<Animator>();
        Health = maxHealth;
    }
    override public void Start() {
        base.Start();
    }
    override public void Update() {
        base.Update();
        if(this.sliding != Vector2Int.zero) {
            movementLerpSpeed = 3f;
        } else {
            movementLerpSpeed = 7f;
        }
    }
    public override void OnDestroy() {
        base.OnDestroy();
    }

    public override void DoTurn() {
        base.DoTurn();
    }

    public bool detectedPlayer = false;
    public Vector2Int sliding = Vector2Int.zero;

    //Movement & Actions
    public virtual bool TargetTile(Vector2Int targetTile) {

        //Find the elements at the target tile
        List<GridElement> tileElements = TileGrid.GetElementsAtTile(targetTile);

        //Check if I can attack someone in this tile
        Character target = CheckAttack(tileElements);
        if (target) {
            Attack(target);
            return true;
        }

        //Check if I can move to this tile
        GridElement floor = CheckMove(tileElements);
        if (floor) {
            Move(floor.tile);
            return true;
        }

        //There is no enemy and there is no floor, nothing I can do with this tile
        return false;
    }
    public virtual Character CheckAttack(List<GridElement> tileElements) {
        //Check for GridElements in the Character layer
        GridElement target = TileGrid.FindElementByLayer(tileElements, GridLayer.Characters);

        if (target != null && target is Character && ((Character)target).team != this.team) {
            //I can attack the target
            return (Character)target;
        }
        //There is no target to attack
        return null;
    }
    public virtual void Attack(Character target) {
        target.ReceiveDamage(ComputeDamage(), this);
        if (ac) {
            Vector2Int dir = target.tile - this.tile;
            if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y)) {
                if (dir.x > 0) {
                    //Right
                    ac.Play("AttackRight");
                } else {
                    //Left
                    ac.Play("AttackLeft");
                }
            } else {
                if (dir.y > 0) {
                    //Up
                    ac.Play("AttackUp");
                } else {
                    //Down
                    ac.Play("AttackDown");
                }
            }
        }
    }
    public virtual int ComputeDamage() {
        int result = Random.Range(strength / 3, strength);
        return result;
    }
    public virtual GridElement CheckMove(List<GridElement> tileElements) {
        GridElement floor = TileGrid.FindElementByLayer(tileElements, GridLayer.Floor);
        GridElement character = TileGrid.FindElementByLayer(tileElements, GridLayer.Characters);
        GridElement wall = TileGrid.FindElementByLayer(tileElements, GridLayer.Wall);
        //If there is floor and there is no obstacle
        if (floor != null && character == null && wall == null) {
            //I can walk to this tile
            return floor;
        } else {
            //I can't walk to this tile
            //Stop sliding in case I was sliding
            sliding = Vector2Int.zero;
            return null;
        }
    }
    public virtual void Move(Vector2Int targetTile) {
        //Animation
        if (ac) {
            Vector2Int dir = targetTile - this.tile;
            if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y)) {
                if (dir.x > 0) {
                    //Right
                    if (this.sliding != Vector2Int.zero) {
                        ac.Play("IdleRight");
                    } else {
                        ac.Play("WalkRight");
                    }
                } else {
                    //Left
                    if (this.sliding != Vector2Int.zero) {
                        ac.Play("IdleLeft");
                    } else {
                        ac.Play("WalkLeft");
                    }
                }
            } else {
                if (dir.y > 0) {
                    //Up
                    if (this.sliding != Vector2Int.zero) {
                        ac.Play("IdleUp");
                    } else {
                        ac.Play("WalkUp");
                    }
                } else {
                    //Down
                    if (this.sliding != Vector2Int.zero) {
                        ac.Play("IdleDown");
                    } else {
                        ac.Play("WalkDown");
                    }
                }
            }
        }
        //Check if I just moved into an ice tile
        if (TileGrid.GetElementAtTile(targetTile, GridLayer.Floor) is IceTile) {
            sliding = targetTile - this.tile;
        } else {
            sliding = Vector2Int.zero;
        }
        TileGrid.MoveElement(this, targetTile);
        //UpdatePlayerVisibility();
    }
    public virtual void Look(Vector2Int targetTile) {

    }
    public virtual bool CanDoSomething(Vector2Int targetTile) {
        List<GridElement> elements = TileGrid.GetElementsAtTile(targetTile);
        return (CheckAttack(elements) || CheckMove(elements));
    }

    //Combat
    public virtual void ReceiveDamage(int dmg, GridElement source) {
        Health -= dmg;
        //Spawn UI Damage
        UIDamage newDamageUI = Instantiate(GM.instance.uiDamagePrefab,GM.instance.canvas.transform);
        newDamageUI.transform.position = Camera.main.WorldToScreenPoint(this.transform.position + Vector3.up * 1.5f);
        newDamageUI.DamageValue = dmg;
        //Play Hurt animation
        ac.Play("Hurt", 1);
    }
    public void ReceiveHealing(int healing) {
        Health += healing;
        //Spawn UI Damage
        UIHealing newHealingUI = Instantiate(GM.instance.uiHealingPrefab, GM.instance.canvas.transform);
        newHealingUI.transform.position = Camera.main.WorldToScreenPoint(this.transform.position + Vector3.up * 1.5f);
        newHealingUI.HealingValue = healing;
    }
    public virtual void Die() {
        //Spawn my corpse
        if (myCorpse) {
            GameObject corpseGameObject = Instantiate(myCorpse, this.transform.position, this.transform.rotation);
            foreach(SpriteRenderer r in corpseGameObject.GetComponentsInChildren<SpriteRenderer>()) {
                r.sortingOrder = (int)(-this.transform.position.z * 100);
            }
        }
        //Destroy myself
        Destroy(this.gameObject);
    }

}
